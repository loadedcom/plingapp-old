*Search it, find it, Pl!ng it!*
Pl!ng is Australia's first Statewide Youth Service Directory.
FEATURES:
Using Pl!ng, you can:
- find services; from youth centres to crisis services for emergencies and everything in between,
- search for a service by using key words, browsing categories, entering a suburb or searching using your own location,
- create a favourites list for services by tapping the star,
- find information on how to get to a service (driving, walking or public transport),
- find contact information like phone numbers and social media,
- read a description of what the service does, 
- find out what age range the service caters to,
- pass on service information by text or email,
- ... and more! 

Service list is Western Australia wide.

This App has been developed by young people and youth sector workers across WA and peak body YACWA.

It is sponsored by WA Government Social Innovations Fund and Lotterywest

Download this App now - keep it for future reference or use it straight away.