/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 */
var Main = require('./App/Components/Main');

import React, {Component} from 'react';
import {AppRegistry, StyleSheet, Text, NavigatorIOS, Navigator, StatusBar, View} from 'react-native';

class Pling extends Component {


  renderScene(route, navigator) {
    var Component = route.component;
    return (
      <View style={{flex:1}}>
        <Component
          route={route}
          navigator={navigator}
          topNavigator={navigator} />
      </View>
    )
  }

  render() {
    StatusBar.setBarStyle('light-content');
    return (
      <Navigator
        sceneStyle={styles.container}
        ref={(navigator) => { this.navigator = navigator; }}
        renderScene={this.renderScene}
        tintColor='#D6573D'
        barTintColor='#9F3AAE'
        titleTextColor='#ffffff'
        navigationBarHidden={true}
        initialRoute={{
          title: 'Pling',
          component: Main,
        }} />
    );
  }
}

const styles = StyleSheet.create({
  navbar: {
    flex: 1,
    alignSelf: 'stretch'
  },
  container: {
    flex: 1,
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});

AppRegistry.registerComponent('Pling', () => Pling);
