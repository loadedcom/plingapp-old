var api = {
  getCategories(){
    // var url = `http://projects.loadedcommunications.com.au/yacwa.org.au/pling/wp-json/wp/v2/service-categories/`;
    var url = `https://www.yacwa.org.au/pling/wp-json/wp/v2/service-categories/?per_page=100`;
    return fetch(url).then((res) => res.json());
  },
  getCategoryThumb(id) {
    var url = `https://www.yacwa.org.au/pling/wp-json/acf/v2/term/service-category/${id}/image`;
    return fetch(url).then((res) => res.json());
  },
  getSingleCategory(category, pageNumber) {
    // var url = `http://projects.loadedcommunications.com.au/yacwa.org.au/pling/wp-json/wp/v2/service?filter[service-category]=${category}`;
    var url = `https://www.yacwa.org.au/pling/wp-json/wp/v2/service?filter[service-category]=${category}&per_page=10&page=${pageNumber}`;
    return fetch(url).then((res) => res.json());
  },
  getService(serviceId) {
    // var url = `http://projects.loadedcommunications.com.au/yacwa.org.au/pling/wp-json/wp/v2/service/${serviceId}`;
    var url = `https://www.yacwa.org.au/pling/wp-json/wp/v2/service/${serviceId}`;
    return fetch(url).then((res) => res.json());
  },
  getServicesByArea(pageNumber, radius, lat, long) {
    var url = `https://www.yacwa.org.au/pling/wp-json/pling/v1/geo-search?distance=${radius}&lat=${lat}&long=${long}&per_page=10&page=${pageNumber}`;
    return fetch(url).then((res) => res.json());
  },
  getServicesBySearch(query) {
    var url = `https://www.yacwa.org.au/pling/wp-json/wp/v2/service?filter[s]=${query}`;
    return fetch(url).then((res) => res.json());
  },
  getPageContent(pageId) {
    var url = `https://www.yacwa.org.au/pling/wp-json/wp/v2/pages/${pageId}`;
    // var url = `http://projects.loadedcommunications.com.au/yacwa.org.au/pling/wp-json/wp/v2/pages/7180`;
    return fetch(url).then((res) => res.json());
  }
}

module.exports = api;
