var React = require('react');
var ReactNative = require('react-native');
var api = require('../Utils/Api');
var Icon = require('react-native-vector-icons/FontAwesome');
var Map = require('./Map');

var {
  StyleSheet,
  Text,
  View,
  PropTypes,
  AlertIOS,
  Linking,
  TouchableHighlight,
  TouchableOpacity
} = ReactNative;

var styles = StyleSheet.create({
  title: {
    fontWeight: '500',
  },
  mapContainer: {
    flex: 1,
  },
  mainContainer: {
    flex: 0.2,
    padding: 30,
    marginTop: 65,
    flexDirection: 'column',
    backgroundColor: '#F5FCFF'
  },
});

class Geolocation extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      position: {
        coords: {
          longitude: 0,
          latitude: 0,
        }
      },
      markers: [{
        latitude: -31.9405,
        longitude: 115.8626,
        title: 'You Are Here',
      }]
    };
  };

  retryAlert(error = false) {
    console.log('LOCALIZATION ERRROR', error);

    AlertIOS.alert(
      'Location Error',
      'Couldn\'t fetch your current location. Retry?',
      [
        {text: 'Yes', onPress: () => this.getCurrentPosition()},
        {text: 'No', onPress: () => console.log('No')},
      ]
    )
  };

  getCurrentPosition() {
    navigator.geolocation.getCurrentPosition(
      location => {
	      this.setState({
          position: location,
        });
      },
      error => {
        this.retryAlert(error)
      },
      {
        enableHighAccuracy: true,
        timeout: 20000,
        maximumAge: 1000
      }
    );
  };

  componentWillMount() {
    this.getCurrentPosition();
  };

  render() {

    var region = {
      latitude: this.state.position.coords.latitude,
      longitude: this.state.position.coords.longitude,
      latitudeDelta: 0.2,
      longitudeDelta: 0.2,
    }

    var linkUrl = `http://maps.apple.com/?saddr=${this.state.position.coords.latitude},${this.state.position.coords.longitude}?daddr=-31.9540,115.8659`;
    var markers = [
        {
          latitude: -31.9540,
          longitude: 115.8659,
        },
        {
          latitude: -31.9640,
          longitude: 115.8669,
        }
    ];

    return (
      <View style={{flex:1}}>
        <View style={styles.mainContainer}>
          <Text style={styles.title}>Current position: {this.state.position.coords.latitude},{this.state.position.coords.longitude}</Text>
        </View>
        <View style={styles.mapContainer}>
          <Map
            markers={markers}
            region={region}
           />
         </View>
      </View>
    );
  }
}

module.exports = Geolocation;
