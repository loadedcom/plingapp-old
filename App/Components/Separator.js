var React = require('react');
var ReactNative = require('react-native');

var {
  View,
  StyleSheet
} = ReactNative;

var styles = StyleSheet.create({
  separator: {
    height: 1,
    backgroundColor: '#E4E4E4'
  }
});

class Separator extends React.Component {
  render() {
    return (
      <View style={styles.separator}></View>
    )
  }
};

module.exports = Separator;
