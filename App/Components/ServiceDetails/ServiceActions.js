var React = require('react');
var ReactNative = require('react-native');

var Fabric = require('react-native-fabric');
var { Answers } = Fabric;
var { Crashlytics } = Fabric;

var {
  StyleSheet,
  propTypes,
  TouchableHighlight,
  TouchableOpacity,
  View,
  Linking,
  Text,
  Image,
  ActionSheetIOS
} = ReactNative;

var styles = StyleSheet.create({
  container: {
    backgroundColor: '#A034B0',
    flex: 1,
    flexDirection: 'row',
    marginBottom: 64
  },
  button: {
    alignItems: 'center',
    backgroundColor: '#A034B0',
    flex: 1,
    flexDirection: 'row',
    padding: 8,
    justifyContent: 'center'
  },
  text: {
    color: '#ffffff',
    fontSize: 16,
    fontWeight: 'bold',
    marginLeft: 10,
  },
  separator: {
    backgroundColor: '#fff',
    height: 36,
    width:1
  }
});

class ServiceActions extends React.Component {
  constructor(props){
    super(props)
  }

  handleLink(url) {
    Linking.openURL(url).catch(err => console.error('An error occurred', err));
  }

  showShareActionSheet(url, title, id) {
    ActionSheetIOS.showShareActionSheetWithOptions({
      url: url,
      message: url,
      subject: title,
    },
    (error) => {
      console.error(error);
    },
    (success, method) => {
      var text;

      if (success) {
        text = 'Answers: Sharing service ' + id + ' by ' + method;
        Answers.logShare(method, 'Service: (' + id.toString() + ') ' + title)
        // text = `Shared via ${method}`;
      } else {
        Answers.logCustom('Cancelled Share Action', {Service: '(' + id.toString() + ') ' + title})
        text = 'Cancelled Share Action: Service: (' + id.toString() + ') ' + title;
      }
      console.log(text);
    });
  }

  render() {
    // console.log(this.props.phoneNumber);
    var emailUrl = this.props.email;

    return (
      <View style={styles.container}>
        <TouchableOpacity
          onPress={this.handleLink.bind(this, this.props.phoneNumber)}
          style={{flex: 1}}
        >
          <View style={styles.button}>
            <Image
              source={require('./images/phone.png')}
              style={{width: 12, height: 12}}
            />
            <Text style={styles.text}>CALL</Text>
          </View>
        </TouchableOpacity>

        <View style={styles.separator}></View>

        <TouchableOpacity
          onPress={this.handleLink.bind(this, this.props.email)}
          style={{flex: 1}}
        >
          <View style={styles.button}>
            <Image
              source={require('./images/email.png')}
              style={{width: 16, height: 10}}
            />
            <Text style={styles.text}>EMAIL</Text>
          </View>
        </TouchableOpacity>

        <View style={styles.separator}></View>

        <TouchableOpacity
          onPress={this.showShareActionSheet.bind(this, this.props.shareUrl, this.props.shareTitle, parseInt(this.props.serviceId))}
          style={{flex: 1}}
        >
          <View style={styles.button}>
            <Image
              source={require('./images/export.png')}
              style={{width: 17, height: 13}}
            />
            <Text style={styles.text}>SHARE</Text>
          </View>
        </TouchableOpacity>
      </View>
    )
  }
}

module.exports = ServiceActions;
