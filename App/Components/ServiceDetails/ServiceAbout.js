var React = require('react');
var ReactNative = require('react-native');
var tags = require('../../Utils/stripTags');

var Entities = require('html-entities').XmlEntities;

var {
  View,
  Text,
  TouchableHighlight,
  PropTypes,
  ScrollView,
  StyleSheet
} = ReactNative;

var styles = StyleSheet.create({
  mainContainer: {
    padding: 20,
    flexDirection: 'column',
  },
  button: {
    backgroundColor: '#8F1F9F',
    color: '#ffffff',
    padding: 20,
  },
  title: {
    color: '#777677',
    fontSize: 20,
    fontWeight: 'bold',
    marginBottom: 20,
  },
  contentCopy: {
    color: '#4F4F4F',
    lineHeight: 23,
  }
})

class ServiceAbout extends React.Component {
  constructor(props){
    super(props)
  }

  render(){
    var renderedContent = Entities.decode(tags.stripTags(this.props.serviceInfo.content.rendered));

    return (
      <View style={styles.mainContainer}>
        <Text style={styles.title}>{Entities.decode(this.props.serviceInfo.title.rendered)}</Text>
        <Text style={styles.contentCopy}>{renderedContent}</Text>
      </View>
    )
  }
}

ServiceAbout.propTypes = {
  serviceInfo: React.PropTypes.object.isRequired,
}

module.exports = ServiceAbout;
