var React = require('react');
var ReactNative = require('react-native');
var ServiceAbout = require('./ServiceAbout');
var ServiceContactInfo = require('./ServiceContactInfo');
var ServiceActions = require('./ServiceActions');
var Map = require('../Map');
var Entities = require('html-entities').XmlEntities;

var Fabric = require('react-native-fabric');
var { Answers } = Fabric;
var { Crashlytics } = Fabric;

var {
  View,
  Text,
  TouchableHighlight,
  ScrollView,
  StyleSheet,
  SegmentedControlIOS,
  ActivityIndicatorIOS,
  Modal,
} = ReactNative;

var styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: '#FAFAFA',
    marginBottom: 20
  },
  segmented: {
    flex: 1,
  },
  mastheadControlsContainer: {
    backgroundColor: '#1CA97C',
    paddingTop: 10,
    paddingBottom: 10,
    paddingLeft: 16,
    paddingRight: 16,
  }
})

class ServiceDetail extends React.Component {
  constructor(props){
    super(props)
    this.state = {
      selectedSegment: 'Info',
      coords: {},
      marker: [],
      phoneNumber: '',
      emailAddress: '',
    }
  }

  createMapCoords() {
    if (!this.props.serviceInfo.acf.address[0].map.lat) {
      console.error('Error!');
    } else {
      var lat = (this.props.serviceInfo.acf.address[0].map.lat);
      var lng = (this.props.serviceInfo.acf.address[0].map.lng);
      this.setState({
        coords: {
          latitude: parseFloat(lat),
          longitude: parseFloat(lng),
          latitudeDelta: 0.2,
          longitudeDelta: 0.2,
        },
        marker: [{
          latitude: parseFloat(lat),
          longitude: parseFloat(lng),
          title: this.props.serviceInfo.title.rendered,
          pinColor: '#8F1F9F',
        }]
      });
    }
  }

  getContactNumberEmail() {
    var email = 'mailto:' + this.props.serviceInfo.acf.generalemail;
    var phoneNumber = 'tel:' + this.props.serviceInfo.acf.telephonenumber1;

    this.setState({
      emailAddress: email,
      phoneNumber: phoneNumber
    })
  }

  componentWillMount(){
    this.createMapCoords();
    this.getContactNumberEmail();
  }

  componentDidMount() {
    console.log('Answers: Service: (' + this.props.serviceInfo.id + ') ' + this.props.serviceInfo.title.rendered)
    Answers.logContentView('Service: (' + this.props.serviceInfo.id + ') ' + this.props.serviceInfo.title.rendered);
  }

  renderPageView() {
    if (this.state.selectedSegment === 'Map') {
      return (
        <Map serviceId={this.props.serviceInfo.id} markers={this.state.marker} region={this.state.coords} width={50} height={50} />
      )
    } else if (this.state.selectedSegment === 'Contact') {
      return (
        <View>
          <ServiceContactInfo serviceInfo={this.props.serviceInfo} />
          <ServiceActions
            serviceId={this.props.serviceInfo.id}
            phoneNumber={this.state.phoneNumber}
            email={this.state.emailAddress}
            shareUrl={this.props.serviceInfo.link}
            shareTitle={Entities.decode(this.props.serviceInfo.title.rendered)}
          />
        </View>
      )
    } else {
      return (
        <View>
          <ServiceAbout serviceInfo={this.props.serviceInfo} />
          <ServiceActions
            serviceId={this.props.serviceInfo.id}
            phoneNumber={this.state.phoneNumber}
            email={this.state.emailAddress}
            shareUrl={this.props.serviceInfo.link}
            shareTitle={Entities.decode(this.props.serviceInfo.title.rendered)}
          />
        </View>
      )
    }
  }

  renderControls() {
    if (this.props.serviceInfo.acf.hide_in_search_by_location) {
      return (
        <SegmentedControlIOS
          values={['Info', 'Contact']}
          selectedIndex={0}
          tintColor={'#ffffff'}
          onValueChange={(val) => {
            this.setState({
              selectedSegment: val
            })
          }}
        />
      )
    } else {
      return (
        <SegmentedControlIOS
          values={['Info', 'Contact', 'Map']}
          selectedIndex={0}
          tintColor={'#ffffff'}
          onValueChange={(val) => {
            this.setState({
              selectedSegment: val
            })
          }}
        />
      )
    }
  }

  render(){
    return (
      // <Actions>
      <ScrollView style={styles.mainContainer}>
        <View style={styles.mastheadControlsContainer}>
          {this.renderControls()}
        </View>
        {this.renderPageView()}
      </ScrollView>
    )
  }
}

ServiceDetail.propTypes = {
  serviceInfo: React.PropTypes.object.isRequired,
}

module.exports = ServiceDetail;
