var React = require('react');
var ReactNative = require('react-native');

var {
  Modal,
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Dimensions
} = ReactNative;

var styles = StyleSheet.create({
  table: {
    flex: 1,
  },
  tableRow: {
    borderBottomColor: '#E2E2E2',
    borderBottomWidth: 1,
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: 4,
    paddingTop: 8,
  },
  tableRowHeader: {
    fontWeight: 'bold',
    fontSize: 12
  },
  closeContainer: {
    alignItems: 'center',
    flex: 1,
    flexDirection: 'row',
    justifyContent:'center',
    marginTop: 10,
  },
  closeButton: {
    color: '#A034B0',
  }
});

class OpeningHours extends React.Component {
  constructor(props) {
    super(props);
  }

  getOpeningHours(){
    var days = [
      {
        name: 'Sunday',
        hours: this.props.openingHours.sunday
      },
      {
        name: 'Monday',
        hours: this.props.openingHours.monday
      },
      {
        name: 'Tuesday',
        hours: this.props.openingHours.tuesday
      },
      {
        name: 'Wednesday',
        hours: this.props.openingHours.wednesday
      },
      {
        name: 'Thursday',
        hours: this.props.openingHours.thursday
      },
      {
        name: 'Friday',
        hours: this.props.openingHours.friday
      },
      {
        name: 'Saturday',
        hours: this.props.openingHours.saturday
      }
    ];

    return days;
  }

  componentDidMount() {
    // this.getOpeningHours();
  }

  render() {

      var {height, width} = Dimensions.get('window');
      var days = this.getOpeningHours();
      var list = days.map((item, index) => {
        var name = days[index].name;
        var hours = days[index].hours;

        if (hours == '25hrs') {
          hours = '24hrs'
        } else if (!hours) {
          hours = 'Closed'
        }

        return (
          <View style={styles.tableRow} key={index}>
            <Text style={styles.tableRowHeader}>{name}</Text>
            <Text style={{color: '#777677', fontSize: 12}}>{hours}</Text>
          </View>
        )
      });



    return (
        <View style={{
          flex: 1,
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent:'center',
        }}>
          <View style={styles.table}>
            {list}
          </View>
        </View>

    )
  }
}

module.exports = OpeningHours;
