var React = require('react');
var ReactNative = require('react-native');

var Icon = require('react-native-vector-icons/FontAwesome');
var OpeningHours = require('./OpeningHours');

var Fabric = require('react-native-fabric');
var { Answers } = Fabric;
var { Crashlytics } = Fabric;

var {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  Linking
} = ReactNative;

var styles = StyleSheet.create({
  section: {
    marginBottom: 20,
  },
  title: {
    color: '#777677',
    fontSize: 20,
    fontWeight: 'bold',
    marginBottom: 16,
    textAlign: 'center',
  },
  contentCopy: {
    color: '#4F4F4F',
    lineHeight: 23,
  },
  openStatus: {
    color: '#649A2A',
    fontSize: 20,
    textAlign: 'center'
  },
  table: {
    flex: 1,
  },
  tableRow: {
    borderBottomColor: '#E2E2E2',
    borderBottomWidth: 1,
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: 4,
    paddingTop: 8,
  },
  tableRowHeader: {
    fontWeight: 'bold',
    fontSize: 14
  },
  addressMapLink: {
    color: '#9F3AAE',
    fontSize: 11,
    marginBottom: 4,
    marginTop: 7,
    textAlign: 'right',
  },
  websiteLink: {
    color: '#9F3AAE',
    marginTop: 7,
    marginBottom: 7,
    textAlign: 'center',
  }
});

class ServiceContactInfo extends React.Component {

  constructor(props){
    super(props);

    this.state = {
      hoursVisible: false
    }
  }

  handleHoursModal() {
    visible = this.state.hoursVisible;

    if (visible) {
      this.setState({
        hoursVisible: false
      })
    } else {
      this.setState({
        hoursVisible: true
      })
    }
  }

  handleLink(url) {
    Linking.openURL(url).catch(err => console.error('An error occurred', err));
  }

  componentDidMount(){
    console.log('Answers: ServiceContactInfo for: (' + this.props.serviceInfo.id + ') ' + this.props.serviceInfo.title.rendered)
    Answers.logContentView('ServiceContactInfo for: (' + this.props.serviceInfo.id + ') ' + this.props.serviceInfo.title.rendered);
  }

  renderPhoneLink() {
    if (this.props.serviceInfo.acf.telephonenumber1) {
      var phoneUrl = 'tel:' + this.props.serviceInfo.acf.telephonenumber1;
      return (
        <View style={{width: 200}}>
          <TouchableOpacity onPress={this.handleLink.bind(this, phoneUrl)}>
            <Text style={{color: '#9F3AAE', textAlign: 'right'}}>{this.props.serviceInfo.acf.telephonenumber1}</Text>
          </TouchableOpacity>
        </View>
      )
    } else {
      return (
        <Text>N/A</Text>
      )
    }
  }

  renderEmailLink() {
    if (this.props.serviceInfo.acf.generalemail) {
      var emailUrl = 'mailto:' + this.props.serviceInfo.acf.generalemail;

      return (
        <View style={{width: 200}}>
          <TouchableOpacity onPress={this.handleLink.bind(this, emailUrl)}>
            <Text style={{color: '#9F3AAE', textAlign: 'right'}}>{this.props.serviceInfo.acf.generalemail}</Text>
          </TouchableOpacity>
        </View>
      )
    } else {
      return (
        <Text>N/A</Text>
      )
    }
  }

  renderMapLink() {
    if (!this.props.serviceInfo.acf.hide_in_search_by_location) {
      var mapsUrl = 'http://maps.apple.com/?q=' + this.props.serviceInfo.acf.address[0].streetno + ' ' +
                    this.props.serviceInfo.acf.address[0].street + ', ' + this.props.serviceInfo.acf.address[0].suburb + ', '
                    + this.props.serviceInfo.acf.address[0].state;

      return (
        <TouchableOpacity onPress={this.handleLink.bind(this, mapsUrl)}>
          <Text style={styles.addressMapLink}>map</Text>
        </TouchableOpacity>
      )
    }
  }

  renderWebLink() {
    if (this.props.serviceInfo.acf.website) {
      return (
        <TouchableOpacity onPress={this.handleLink.bind(this, this.props.serviceInfo.acf.website)}>
          <Text style={styles.websiteLink}>{this.props.serviceInfo.acf.website}</Text>
        </TouchableOpacity>
      )
    }
  }

  renderAddress() {
    if (this.props.serviceInfo.acf.address[0].street) {
      return (
        <View style={styles.tableRow}>
          <Text style={styles.tableRowHeader}>Address</Text>
          <View style={{width:200}}>
            <Text style={{color: '#777677', textAlign: 'right'}}>
              {this.props.serviceInfo.acf.address[0].streetno} {this.props.serviceInfo.acf.address[0].street}
            </Text>
            <Text style={{color: '#777677', textAlign: 'right'}}>
              {this.props.serviceInfo.acf.address[0].suburb}
            </Text>
            <Text style={{color: '#777677', textAlign: 'right'}}>
              {this.props.serviceInfo.acf.address[0].state} {this.props.serviceInfo.acf.address[0].postcode}
            </Text>
            {this.renderMapLink()}
          </View>
        </View>
      )
    }
  }

  render() {



    return (
      <View style={{padding: 20}}>

        <View style={styles.section}>
          <Text style={styles.title}>
            CONTACT DETAILS
          </Text>

          <View style={styles.table}>
            <View style={styles.tableRow}>
              <Text style={styles.tableRowHeader}>Phone</Text>
              {this.renderPhoneLink()}
            </View>
            <View style={styles.tableRow}>
              <Text style={styles.tableRowHeader}>Email</Text>
              {this.renderEmailLink()}
            </View>
            {this.renderAddress()}
          </View>

          {this.renderWebLink()}

        </View>

        <View style={styles.section}>
          <View style={{flex: 1, flexDirection: 'row', justifyContent: 'center'}}>
            <Text style={styles.title}>
              OPENING HOURS
            </Text>
          </View>

          <OpeningHours openingHours={this.props.serviceInfo.acf} />
        </View>

      </View>
    )
  }
};

module.exports = ServiceContactInfo;
