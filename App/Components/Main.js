var React = require('react');
var ReactNative = require('react-native');
var Categories = require('./Browse/Categories');
var NearMe = require('./NearMe');
var Geolocation = require('./Geolocation');
var ContentPage = require('./ContentPage');
var Separator = require('./Separator');
var Icon = require('react-native-vector-icons/Ionicons');
var api = require('../Utils/Api');

var Fabric = require('react-native-fabric');
var { Answers } = Fabric;
var { Crashlytics } = Fabric;

var {
  View,
  Component,
  Text,
  StyleSheet,
  TouchableHighlight,
  NavigatorIOS,
  TabBarIOS,
  Navigator,
  ActivityIndicatorIOS
} = ReactNative;

var styles = StyleSheet.create({
})

class Main extends React.Component {

  constructor(props){
    super(props);
    this.state = {
      selectedTab: 'browse',
    }
  }

  render(){

    return (
      <TabBarIOS>
        <Icon.TabBarItemIOS
          title="Services"
          ref='browseTabBar'
          selected={this.state.selectedTab === 'browse'}
          iconName="ios-list-outline"
          onPress={() => {
            if (this.state.selectedTab !== 'browse') {
              this.setState({
                selectedTab: 'browse'
              });
            } else if (this.state.selectedTab === 'browse') {
              this.refs.browseRef.popToTop();
            }
          }}>
            <NavigatorIOS
              style={{flex:1}}
              barTintColor='#9F3AAE'
              tintColor='#ffffff'
              titleTextColor='#ffffff'
              ref='browseRef'
              translucent={false}
              initialRoute={{
                component: Categories,
                title: 'Pling',
              }}
            />
        </Icon.TabBarItemIOS>
        <Icon.TabBarItemIOS
          selected={this.state.selectedTab === 'geo'}
          iconName="ios-navigate-outline"
          title="Near Me"
          onPress={() => {
            if (this.state.selectedTab !== 'geo') {
              this.setState({
                selectedTab: 'geo'
              });
            } else if (this.state.selectedTab === 'geo') {
              this.refs.browseRef.popToTop();
            }
          }}>
            <NavigatorIOS
              style={{flex:1}}
              ref='geoRef'
              barTintColor='#9F3AAE'
              tintColor='#ffffff'
              titleTextColor='#ffffff'
              translucent={false}
              initialRoute={{
                component: NearMe,
                title: 'Near Me',
              }}
            />
        </Icon.TabBarItemIOS>
        <Icon.TabBarItemIOS
          selected={this.state.selectedTab === 'more'}
          iconName="ios-information"
          title="About"
          onPress={() => {
                this.setState({
                    selectedTab: 'more',
                });
          }}>
            <NavigatorIOS
              style={{flex:1}}
              ref='aboutRef'
              barTintColor='#9F3AAE'
              tintColor='#ffffff'
              titleTextColor='#ffffff'
              translucent={false}
              initialRoute={{
                component: ContentPage,
                title: 'About',
                passProps: {
                  title: 'About',
                  pageId: 7180
                }
              }}
            />
        </Icon.TabBarItemIOS>
        <Icon.TabBarItemIOS
          selected={this.state.selectedTab === 'help'}
          iconName="ios-help-outline"
          title="In Crisis?"
          onPress={() => {
                this.setState({
                    selectedTab: 'help',
                });
          }}>
            <NavigatorIOS
              style={{flex:1}}
              ref='aboutRef'
              barTintColor='#9F3AAE'
              tintColor='#ffffff'
              titleTextColor='#ffffff'
              translucent={false}
              initialRoute={{
                component: ContentPage,
                title: 'Need Help?',
                passProps: {
                  title: 'Help',
                  pageId: 7177
                }
              }}
            />
        </Icon.TabBarItemIOS>
      </TabBarIOS>
    )
  }
}



Main.propTypes = {
  navigator: React.PropTypes.object.isRequired,
};

module.exports = Main;
