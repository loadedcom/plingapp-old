var React = require('react');
var ReactNative = require('react-native');
import Icon from 'react-native-vector-icons/FontAwesome';

var {
  View,
  ListView,
  StyleSheet,
  TouchableOpacity,
  Text,
} = ReactNative;

var styles = StyleSheet.create({
  flexContainer: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent:'center',
  },
});

class MapDetailCallout extends React.Component {
  constructor(props) {
    super(props)
  }

  render() {
    return (
      <View style={styles.flexContainer}>
        <TouchableOpacity onPress={this.props.action} navigator={this.props.navigator}>
          <View style={styles.flexContainer}>
            <View style={{width: 120, marginRight: 10}}>
              <Text style={{ color: '#000'}}>{this.props.title}</Text>
            </View>
            <Icon name="angle-right" size={30} color="#06AA7B" />
          </View>
        </TouchableOpacity>
      </View>
    )
  }
}

module.exports = MapDetailCallout;
