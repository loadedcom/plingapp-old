var React = require('react');
var ReactNative = require('react-native');
var Icon = require('react-native-vector-icons/Entypo');
var SearchResults = require('./Browse/SearchResults')

var {
  StyleSheet,
  View,
  TouchableOpacity,
  TextInput
} = ReactNative;


var styles = StyleSheet.create({
  container: {
    height: 40,
    padding:6,
    backgroundColor: '#1CA97C',
    flex:1,
    flexDirection: 'row',
  },
  searchInputContainer: {
    backgroundColor: '#2B9170',
    flex: 1,
    height:28,
  },
  searchInput: {
    color: '#fff',
    fontSize: 12,
    height: 28,
    textAlign: 'left',
    paddingLeft: 6
  },
  iconContainerSearch: {
    backgroundColor: '#2B9170',
    borderTopLeftRadius: 4,
    borderBottomLeftRadius: 4,
    paddingTop: 6,
    paddingLeft: 6,
    height: 28,
  },
  iconContainerClose: {
    backgroundColor: '#2B9170',
    borderTopRightRadius: 4,
    borderBottomRightRadius: 4,
    paddingTop: 6,
    paddingRight: 6,
    height: 28,
  },

})


class SearchBar extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      searchQuery: ''
    }
  }

  handleSubmit() {
    this.props.navigator.push({
      title: 'Search Results',
      backButtonTitle: 'Back',
      component: SearchResults,
      passProps: {
        searchQuery: this.state.searchQuery
      }
    });
  }

  render() {
    return (

      <View style={styles.container}>
        <TouchableOpacity
          activeOpacity={1}
          style={styles.iconContainerSearch}
        >
          <Icon name="magnifying-glass" size={14} color="#97D5C1" />
        </TouchableOpacity>
        <View style={styles.searchInputContainer}>
          <TextInput
            style={styles.searchInput}
            placeholder='Search'
            placeholderTextColor='#97D5C1'
            enablesReturnKeyAutomatically={true}
            returnKeyType='search'
            autoCorrect={false}
            clearButtonMode='never'
            onSubmitEditing={this.handleSubmit.bind(this)}
            onChangeText={(text) => this.setState({searchQuery: text})}
          />
        </View>
        <TouchableOpacity
          activeOpacity={1}
          style={styles.iconContainerClose}

        >
          <Icon name="circle-with-cross" size={14} color="#97D5C1" />
        </TouchableOpacity>
      </View>
    )
  }
}

SearchBar.propTypes = {
  navigator: React.PropTypes.object.isRequired,
};

module.exports = SearchBar;
