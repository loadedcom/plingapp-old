var React = require('react');
var ReactNative = require('react-native');
var Icon = require('react-native-vector-icons/Ionicons');

var {
  Modal,
  View,
  ActivityIndicatorIOS,
  Text,
  TouchableOpacity
} = ReactNative;

class LoadingModal extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    var renderCancelButton;

    if (this.props.showCancelButton) {
      renderCancelButton = <CancelButton action={this.props.cancelAction} />;
    }


    return (

      <Modal
        visible={this.props.loading}
        transparent={true}
      >
        <View style={{
          flex: 1,
          flexDirection: 'column',
          alignItems: 'center',
          justifyContent:'center',
          backgroundColor: 'rgba(0,0,0,0.6)'
        }}>
          <View style={{
            backgroundColor: '#92369F',
            borderRadius: 10,
            padding: 10,
            width: 100
          }}>
            <ActivityIndicatorIOS
              animating={this.props.loading}
              color="#fff"
              size="large"
            />
            <Text style={{
              textAlign: 'center',
              color: '#fff',
              marginTop: 10
            }}>Loading...</Text>
          </View>

          <View style={{
            height: 20,
            marginTop: 10,
            width: 70
          }}>
            {renderCancelButton}
          </View>
        </View>
      </Modal>

    )
  }
}

class CancelButton extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <TouchableOpacity
        onPress={this.props.action}
      >
        <View style={{
          alignItems: 'center',
          backgroundColor: 'rgba(255,255,255,0.3)',
          borderRadius: 10,
          flex: 1,
          flexDirection: 'row',
          justifyContent: 'center',
          padding: 5,

        }}>
          <Icon name="ios-close" size={23} color="#ffffff" />
          <Text style={{fontSize: 10, marginLeft: 4,color: '#ffffff'}}>Cancel</Text>
        </View>
      </TouchableOpacity>
    )
  }
}

module.exports = LoadingModal;
