var React = require('react');
var ReactNative = require('react-native');
var MapView = require('react-native-maps');
var MapDetailCallout = require('./MapDetailCallout');

var Fabric = require('react-native-fabric');
var { Answers } = Fabric;
var { Crashlytics } = Fabric;

var {
  StyleSheet,
  AlertIOS,
  propTypes,
  View,
  Dimensions,
  Text,
  Linking,
  AlertIOS,
  TouchableHighlight
} = ReactNative;

var {height, width} = Dimensions.get('window');

var styles = StyleSheet.create({
  mapContainer: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: '#F5FCFF'
  },
  map: {
    flex:1,
    width: (1 * width),
    height: (1 * height)
  },
});


class Map extends React.Component {
  constructor(props){
    super(props)
  }

  handleLink(lat, lng) {
    var url = 'http://maps.apple.com/?q=' + lat + ',' + lng;
    Linking.openURL(url).catch(err => console.error('An error occurred', err));
  }

  componentDidMount() {
    if (this.props.serviceId) {
      console.log('Answers: Loading Map for: ' + this.props.serviceId)
      Answers.logContentView('Map: ' + this.props.serviceId.toString());
    }
  }

  render(){
    return (
      <View style={styles.mapContainer}>
        <MapView
          style={styles.map}
          initialRegion={{
            latitude: this.props.region.latitude,
            longitude: this.props.region.longitude,
            latitudeDelta: 0.2,
            longitudeDelta: 0.2,

          }}
          onRegionChange={this.props.handleRegionChange}
        >
        {this.props.markers.map((marker, index) => (
          <MapView.Marker
            key={index}
            coordinate={{
              latitude: marker.latitude,
              longitude: marker.longitude,
            }}
            title={marker.title}
            pinColor={marker.pinColor}
          >
            <MapView.Callout>
              <MapDetailCallout
                action={this.handleLink.bind(this, marker.latitude, marker.longitude)}
                title={marker.title}
              />
            </MapView.Callout>
          </MapView.Marker>
        ))}
        </MapView>
      </View>
    )
  }
}

Map.propTypes = {
  region: React.PropTypes.object.isRequired,
  markers: React.PropTypes.array.isRequired,
};

module.exports = Map;
