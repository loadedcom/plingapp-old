var React = require('react');
var ReactNative = require('react-native');

var ResponsiveImage = require('react-native-responsive-image');

var {
  View,
  Component,
  Text,
  StyleSheet,
  TouchableOpacity,
  Dimensions,
  Image,
  ActivityIndicatorIOS
} = ReactNative;

var styles = StyleSheet.create({
  mainContainer: {
    alignItems: 'center',
    flex: 1,
    flexDirection: 'row',
    justifyContent:'center',
  },
  flexColumnContainer: {
    flex: 1,
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'center',
    width: 180
  },
  text: {
    color: '#8B8B8B',
    fontSize: 12,
    marginBottom: 10,
    marginTop: 10,
    textAlign: 'center'
  },
  button: {
    borderColor: '#8E8E8E',
    borderWidth: StyleSheet.hairlineWidth,
    borderRadius: 5,
    padding: 15,
    paddingTop: 5,
    paddingBottom: 5,
    marginTop: 10,
  },
})

class ErrorOffline extends React.Component {
  constructor(props) {
    super(props)
  }

  render() {
    return (
      <View style={styles.mainContainer}>
        <View style={styles.flexColumnContainer}>
          <ResponsiveImage
            style={{marginBottom: 10}}
            source={require('./offline.png')}
            initWidth="106"
            initHeight="82"
          />
          <View style={styles.flexColumnContainer}>
            <Text style={[{fontWeight: 'bold'}, styles.text]}>Network Error</Text>
            <Text style={styles.text}>{this.props.message}</Text>
          </View>
          <View style={styles.flexColumnContainer}>
            <TouchableOpacity
              style={styles.button}
              onPress={this.props.retryAction}
            >
              <Text style={{color: '#8B8B8B', fontSize: 12}}>Tap to retry</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    )
  }

}

module.exports = ErrorOffline;
