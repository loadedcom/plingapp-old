var React = require('react');
var ReactNative = require('react-native');
var LoadingModal = require('../../LoadingModal');

var ResponsiveImage = require('react-native-responsive-image');
import Icon from 'react-native-vector-icons/FontAwesome';


var {
  StyleSheet,
  Text,
  ScrollView,
  View,
  PropTypes,
  AlertIOS,
  TouchableHighlight,
  ActivityIndicatorIOS,
  ScrollView,
  TouchableOpacity,
  Image,
  Dimensions,
  Modal
} = ReactNative;

var {height, width} = Dimensions.get('window');

var styles = StyleSheet.create({
  flexContainer: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent:'center',
  },
  mainContainer: {
    backgroundColor: '#FAFAFA'
  },
  card: {
    backgroundColor: '#fff',
    borderRadius: 2,
    flex: 1,
    margin: 20,
    padding: 10,
    shadowColor: '#000',
    shadowOffset: {
      x: 0,
      y: 3,
    },
    shadowOpacity: .13,
    shadowRadius: 4,
    width: (0.9 * width)
  },
  text: {
    color: '#4A4A4A',
    fontSize: 14,
    marginBottom: 10,
    textAlign: 'center',
  },
  buttonContainer: {
    flexDirection: 'column',
    margin: 24,
    marginLeft: 0,
    marginRight: 0,
  },
  button: {
    backgroundColor: '#A034B0',
    borderRadius: 4,
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent:'center',
    padding: 8,
    width: 120,
  },
  buttonOk: {
    backgroundColor: '#06AA7B',
    marginTop: 10,
    padding: 14,
  },
  buttonText: {
    color: '#fff',
    fontSize: 12,
    marginRight: 10,
    textAlign: 'center'
  }
});


class LocationPermissionsError extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    var renderContent;

    if (this.props.checking) {
      renderContent = <PermissionsSpinner />
    } else {
      renderContent = <ErrorContent action={this.props.handleCheckPermissions} permissionsOpened={this.props.permissionsOpened} handleCancel={this.props.handleCancel} handleFixed={this.props.handleFixed} />
    }

    return (
      <Modal
        transparent={true}
      >
        {renderContent}
      </Modal>
    )
  }

}

class ErrorContent extends React.Component {
  constructor(props) {
    super(props);
  }

  renderActionButtons() {
    if (this.props.permissionsOpened) {
      return (
        <TouchableHighlight style={[styles.button, styles.buttonOk]} onPress={this.props.handleFixed}>
          <View style={{flex:1, flexDirection: 'row'}}>
            <Text style={styles.buttonText}>OK, I fixed it</Text>
          </View>
        </TouchableHighlight>
      )
    } else {
      return (
        <TouchableHighlight
          style={styles.button}
          onPress={this.props.action}
        >
          <View style={{flex:1, flexDirection: 'row'}}>
            <Text style={styles.buttonText}>Open Settings</Text>
            <Icon name="external-link" size={12} color="#fff" />
          </View>
        </TouchableHighlight>
      )
    }
  }

  render() {

    var renderActionButtons = this.renderActionButtons();

    return (
        <View style={[{
          backgroundColor: 'rgba(0,0,0,0.6)'
        },styles.flexContainer]}>
          <View style={styles.card}>
            <Text style={styles.text}>
              We need to access your location to determine services in your area.
            </Text>

            <Text style={styles.text}>
              Please allow Pling to access your location While Using the App in Settings.
            </Text>

            <View style={{flex: 1, flexDirection: 'row', justifyContent: 'center',}}>
              <ResponsiveImage
                style={{borderWidth: 1,borderColor: '#D1D0D0'}}
                source={require('./location-permissions-screenshot.png')}
                initWidth="215"
                initHeight="177"
              />
            </View>

            <View style={[styles.flexContainer,styles.buttonContainer]}>

              {renderActionButtons}

              <TouchableHighlight
                onPress={this.props.handleCancel}
              >
                <Text style={{fontSize: 10, color: '#B06EB2', marginTop: 10}}>Maybe later</Text>
              </TouchableHighlight>
            </View>
          </View>
        </View>
    )
  }
}

class PermissionsSpinner extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <View style={{
        flex: 1,
        backgroundColor: 'rgba(0,0,0,0.6)'
      }}>
        <View
          style={{
            flex: 1,
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent:'center',
          }}
        >
          <View style={styles.card}>
            <ActivityIndicatorIOS
              animating={this.props.spinnerVisible}
              color="#000"
              size="large"
            />
            <Text style={styles.text}>Checking Permissions...</Text>
          </View>
        </View>
      </View>
    )
  }
}


module.exports = LocationPermissionsError;
