var React = require('react');
var ReactNative = require('react-native');
var LoadingModal = require('./LoadingModal');

var Entities = require('html-entities').XmlEntities;
var tags = require('../Utils/stripTags');
var api = require('../Utils/Api');

var ErrorOffline = require('./Errors/Offline/ErrorOffline');

var Fabric = require('react-native-fabric');
var { Answers } = Fabric;
var { Crashlytics } = Fabric;

var {
  View,
  Text,
  TouchableHighlight,
  PropTypes,
  ScrollView,
  StyleSheet
} = ReactNative;

var styles = StyleSheet.create({
  mainContainer: {
    backgroundColor: '#FBFBFB',
    flexDirection: 'column',
    padding: 20,
  },
  button: {
    backgroundColor: '#8F1F9F',
    color: '#ffffff',
    padding: 20,
  },
  title: {
    color: '#777677',
    fontSize: 20,
    fontWeight: 'bold',
    marginBottom: 20,
  },
  contentCopy: {
    color: '#4F4F4F',
    lineHeight: 23,
  }
});

class Title extends React.Component {
  constructor(props){
    super(props)
  }

  render() {
    return (
      <View>
        <Text style={styles.title}>{this.props.title}</Text>
      </View>
    )
  }
}

class Content extends React.Component {
  constructor(props){
    super(props)
  }

  render() {
    renderedContent = tags.stripTags(this.props.content);

    return (
      <View>
        <Text style={styles.contentCopy}>{renderedContent}</Text>
      </View>
    )
  }
}

class ContentPage extends React.Component {
  constructor(props){
    super(props)

    this.state = {
      isLoading: true,
      error: false,
      content: ''
    }
  }

  cancelFetchRequest() {
    this.setState({
      showCancelButton: false,
      isLoading: false,
    });

    console.log('Logging to Answers: Cancelled fetch: ContentPage ' + this.props.title)
    Answers.logCustom('Cancelled Fetch', {Component: 'ContentPage ' + this.props.title});
  }

  retryFetchRequest() {
    this.setState({
      showCancelButton: false,
      isLoading: false,
    });
    setTimeout(() => {this.getPage()}, 100);
  }

  RenderNetworkError() {
    if (this.state.networkError) {
      return (
        <View style={{marginTop: -100}}>
          <ErrorOffline retryAction={() => this.getPage()} message="Your connection to the server was lost"></ErrorOffline>
        </View>
      )
    }
  }

  getPage() {
    this.setState({
      isLoading: true
    });

    setTimeout(() => {this.setState({showCancelButton: true})}, 5000);

    api.getPageContent(this.props.pageId)
    .then((res) => {
      this.setContentState(res);
      Answers.logContentView('ContentPage: ' + this.props.pageId);
    })
    .catch(
      (e) => {
        console.log(e);
        this.cancelFetchRequest();
        this.setState({
          networkError: true
        });
      }
    )
  }

  setContentState(post){
    this.setState({
      title: post.title.rendered,
      content: post.content.rendered,
      isLoading: false
    });
  }

  componentDidMount() {
    this.getPage();
  }

  render(){
    return (
      <ScrollView style={styles.mainContainer}  contentContainerStyle={{flex: 1, flexDirection: 'column', justifyContent: 'center', marginBottom: 50}}>
        <LoadingModal loading={this.state.isLoading} cancelAction={this.cancelFetchRequest.bind(this)} showCancelButton={this.state.showCancelButton} />

        <Title title={this.state.title} />
        <Content content={Entities.decode(this.state.content)} />

        {this.RenderNetworkError()}

      </ScrollView>
    )
  }
}

ContentPage.propTypes = {
  pageId: React.PropTypes.number.isRequired,
}

module.exports = ContentPage;
