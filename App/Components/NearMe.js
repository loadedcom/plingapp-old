var React = require('react');
var ReactNative = require('react-native');
var Icon = require('react-native-vector-icons/FontAwesome');
var _ = require('underscore');
var MapView = require('react-native-maps');
const Permissions = require('react-native-permissions');
var RNUtils = require('react-native-utils');
var reactMixin = require('react-mixin');
import TimerMixin from 'react-timer-mixin';
var Entities = require('html-entities').XmlEntities;

var api = require('../Utils/Api');
var LoadingModal = require('./LoadingModal');

var Map = require('./Map');
var MapDetailCallout = require('./MapDetailCallout');
var RangeSlider = require('./RangeSlider');

var ServiceRow = require('./Browse/ServiceRow');
var ServiceDetail = require('./ServiceDetails/ServiceDetail');
var ListFooter = require('./Browse/ListFooter');
var Separator = require('./Separator');

var ErrorOffline = require('./Errors/Offline/ErrorOffline');
var LocationNotFound = require('./Errors/LocationNotFound/LocationNotFound');
var LocationPermissionsError = require('./Errors/LocationPermissions/ErrorLocationPermissions');

var Fabric = require('react-native-fabric');
var { Answers } = Fabric;
var { Crashlytics } = Fabric;

var {
  StyleSheet,
  Text,
  View,
  PropTypes,
  AlertIOS,
  Linking,
  TouchableHighlight,
  SegmentedControlIOS,
  ScrollView,
  TouchableOpacity,
  ListView,
  Dimensions
} = ReactNative;

var {height, width} = Dimensions.get('window');


var styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    backgroundColor: '#FAFAFA'
  },
  segmented: {
    flex: 1,
  },
  mastheadControlsContainer: {
    backgroundColor: '#1CA97C',
    paddingTop: 10,
    paddingBottom: 10,
    paddingLeft: 16,
    paddingRight: 16,
  },
  mapContainer: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: '#F5FCFF'
  },
  map: {
    flex:1,
    width: (1 * width),
    height: (1 * height)
  },
});

class NearMe extends React.Component {
  constructor(props) {
    super(props);

    var ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});

    this.state = {
      selectedSegment: 'List',
      coords: {
        latitude: 0,
        longitude: 0,
        latitudeDelta: 0.2,
        longitudeDelta: 0.2,
      },
      markers: [{
        latitude: -31.9405,
        longitude: 115.8626,
        title: 'You Are Here',
      }],
      sliderValue: 15,
      isCheckingPermissions: false,
      locationPermission: '',
      requiresPermissions: false,
      permissionsOpened: false,
      isLoading: false,
      loadingMore: false,
      errorType: '',
      queryNumber: 1,
      data: [],
      listFooterVisible: false,
      dataSource: new ListView.DataSource({
        rowHasChanged: (row1, row2) => row1 !== row2,
      }),
    };
  };

  renderError() {
    if (this.state.errorType == 'locationPermissions' && this.state.requiresPermissions) {
      return (
        <LocationPermissionsError
          checking={this.state.isCheckingPermissions}
          permissionsOpened={this.state.permissionsOpened}
          handleCheckPermissions={()=> this._openPermissionsSettings()}
          handleCancel={this._cancelPermissionsCheck.bind(this)}
          handleFixed={this._handlePermissionsCheck.bind(this)}
        />
      )
    } else if (this.state.errorType == 'locationNotFound') {
      return (
        <LocationNotFound retryAction={() => this._handlePermissionsCheck()} message="Your location could not be determined"></LocationNotFound>
      )
    } else if (this.state.errorType == 'networkError') {
      return (
        <ErrorOffline retryAction={() => this.getPosts()} message="Your connection to the server was lost"></ErrorOffline>
      )
    }
  }

  cancelFetchRequest() {
    this.setState({
      showCancelButton: false,
      isLoading: false,
    });
    console.log('Logging to Answers: Cancelled fetch request: NearMe')
    Answers.logCustom('Cancelled fetch request: NearMe');
  }

  retryFetchRequest() {
    this.setState({
      showCancelButton: false,
      isLoading: false,
    });
    setTimeout(() => {this.getCurrentPosition()}, 100);
  }

  _permissionsDenied() {
    this.setState({
      isCheckingPermissions: false,
      permissionsOpened: false,
      errorType: 'locationNotFound'
    })
    console.log('Logging to Answers: Denied location permissions.')
    Answers.logCustom('Denied location permissions.');
  }

  _openPermissionsSettings() {
    RNUtils.openSettings();

    setTimeout(() => {
      this.setState({
        permissionsOpened: true
      })
    }, 500);

    console.log('Logging to Answers: Opened Settings.app')
    Answers.logCustom('Opened Settings');
  }

  _handlePermissionsCheck() {
    this.setState({isCheckingPermissions: true})

    setTimeout(() => {
      Permissions.getPermissionStatus('location')
      .then(response => {
        //response is one of: 'authorized', 'denied', 'restricted', or 'undetermined'
        if (response == 'authorized') {
          this.setState({
            isCheckingPermissions: false,
            errorType: ''
          })
          console.log('Logging to Answers: Accepted location permissions')
          Answers.logCustom('Accepted location permissions.');
          this.getCurrentPosition();
        } else {
          setTimeout(() => {
            this.setState({
              requiresPermissions: true,
              isCheckingPermissions: false,
              permissionsOpened: false,
              errorType: 'locationPermissions'
            });
            this.renderError();
          }, 500);
        }
      });
    }, 500);
  }

  _cancelPermissionsCheck() {
    this.setState({
      isCheckingPermissions: false,
      locationPermission: 'denied',
      errorType: 'locationNotFound',
    });

    console.log('Logging to Answers: Check permissions later')
    Answers.logCustom('Check permissions later')
  }

  retryAlert(error = false) {
    console.log('LOCATION ERRROR', error);

    AlertIOS.alert(
      'Location Error',
      'Couldn\'t fetch your current location. Retry?',
      [
        {text: 'Yes', onPress: () => this._handlePermissionsCheck()},
        {text: 'No', onPress: () => this._permissionsDenied()},
      ]
    )
  };

  getCurrentPosition() {
    navigator.geolocation.getCurrentPosition(
      location => {
        this.setState({
          coords: location.coords
          // coords: {
          //   latitude: -31.9535960,
          //   longitude: 115.8570120,
          // },
        });
        this.getPosts(1, this.state.sliderValue);
      },
      error => {
        this.setState({
          renderError: 'locationPermissions',
          requiresPermissions: true
        })
      },
      {
        enableHighAccuracy: true,
        timeout: 20000,
        maximumAge: 1000
      }
    );

  };

  getPosts(pageNumber, radius) {
    var totalPosts;

    this.setState({
      isLoading: true,
    });

    setTimeout(() => {this.setState({showCancelButton: true})}, 5000);

    // api.getServicesByArea(this.state.queryNumber, this.state.sliderValue, this.state.coords.latitude, this.state.coords.longitude)
    // // api.getServicesByArea(this.state.queryNumber, this.state.sliderValue, -31.9535960, 115.8570120)
    fetch(`https://www.yacwa.org.au/pling/wp-json/pling/v1/geo-search?distance=${this.state.sliderValue}&lat=${this.state.coords.latitude}&long=${this.state.coords.longitude}&per_page=10&page=${this.state.queryNumber}`)
    // fetch('https://www.yacwa.org.au/pling/wp-json/pling/v1/geo-search?distance=15&lat=-31.9535960&long=115.8570120&per_page=10&page=1')
    .then(function(res) {
      if (res.status == 200) {
        totalPosts = res.headers.get('X-WP-Total');
        return res.json();
      }
    })
    .then((data) => {
      if (data.length == 0) {
        this.setState({
          isLoading: false,
          error: 'no posts found'
        })
        console.log(this.state.error);
      } else {
        this.setState({
          isLoading: false,
          dataSource: this.state.dataSource.cloneWithRows(data),
          data: data,
          numberOfPosts: totalPosts,
          showingNumberOfPosts: data.length,
          listFooterVisible: true,
        });
      }
    })
    .catch(
      (e) => {
        console.log(e);
        this.cancelFetchRequest();
        this.setState({
          errorType: 'networkError',
          networkError: true
        });
      }
    )
  };

  getMorePosts(pageNumber, radius) {
    this.setState({
      loadingMore: true,
    });
    console.log('getting page:' + this.state.queryNumber );

    api.getServicesByArea(this.state.queryNumber, this.state.sliderValue, this.state.coords.latitude, this.state.coords.longitude)
    // api.getServicesByArea(this.state.queryNumber, this.state.sliderValue, -31.9535960, 115.8570120)

    .then((res) => {

      if (res.length == 0) {
        this.setState({
          loadingMore: false,
          error: 'no posts found'
        })
        console.log(this.state.error);
      } else {
        var oldList = this.state.data;

        var newList = oldList.concat(res);

        this.setState({
          dataSource: this.state.dataSource.cloneWithRows(newList),
          data: newList,
          showingNumberOfPosts: newList.length,
          queryNumber: this.state.queryNumber + 1,
          loadingMore: false
        });
      }
    })
  }

  setPostsListState(posts){
    this.setState({
      isLoading: false
    });

    return
  }

  handleSliderChange(sliderValue) {
    this.getPosts(1, this.state.sliderValue);
    this.setState({
      sliderValue: sliderValue
    });
    console.log('Searching for services within ' + this.state.sliderValue +'km')
  }

  handleValueChange(sliderValue) {
    this.setState({
      sliderValue: sliderValue
    });
  }

  handleSelectArticle(service) {
    this.props.navigator.push({
      title: Entities.decode(service.title.rendered),
      backButtonTitle: 'Back',
      component: ServiceDetail,
      passProps: {
        serviceInfo: service
      }
    });
  }

  handleMarkerPress() {
    AlertIOS.alert(
      'Hi',
      'Retry?',
      [
        {text: 'Yes', onPress: () => console.log('yes')},
        {text: 'No', onPress: () => console.log('No')},
      ]
    )
  }

  requestMorePosts() {
    if (!this.state.loadingMore) {
      this.getMorePosts();
    }
  }

  renderRow(service) {
    return (
      <ServiceRow
        key={service.id}
        action={this.handleSelectArticle.bind(this, service)}
        postInfo={service}
        enableDistanceIcon={true}
        mylat={this.state.coords.latitude}
        mylng={this.state.coords.longitude}
      />
    )
  }

  componentWillMount(){
    Permissions.getPermissionStatus('location')
    .then(response => {
      //response is one of: 'authorized', 'denied', 'restricted', or 'undetermined'
      if (response == 'authorized') {
        this.setState({
          locationPermission: response,
          sliderValue: 15,
        });

        this.getCurrentPosition()
      } else {
        this.getCurrentPosition()
      }
    })
    .then(
    );
  }

  componentDidMount() {
    this._handlePermissionsCheck()
  }

  renderFooterPls() {
    if (this.state.listFooterVisible) {
      return (
        <ListFooter
          loadingMore={this.state.loadingMore}
          totalPostsFound={this.state.numberOfPosts}
          showingNumberOfPosts={this.state.showingNumberOfPosts}
          action={this.requestMorePosts.bind(this)}
        />
      )
    }
  }

  renderPageView() {
    if (this.state.selectedSegment === 'Map') {

      var posts = this.state.data;

      var markers = [];

      return (
        <View style={styles.mapContainer}>
          <MapView
            style={styles.map}
            showsUserLocation={true}
            loadingEnabled={true}
            initialRegion={{
              latitude: this.state.coords.latitude,
              longitude: this.state.coords.longitude,
              latitudeDelta: 0.2,
              longitudeDelta: 0.2,

            }}
          >
          {posts.map((item, index) => {
            return (
              <MapView.Marker
                key={index}
                coordinate={{
                  latitude: parseFloat(posts[index].acf.address[0].map.lat),
                  longitude: parseFloat(posts[index].acf.address[0].map.lng),
                }}
                title={posts[index].title.rendered}
                pinColor="#8F1F9F"
              >
                <MapView.Callout>
                  <MapDetailCallout
                    action={this.handleSelectArticle.bind(this, posts[index])}
                    navigator={this.props.navigator}
                    title={Entities.decode(posts[index].title.rendered)}
                  />
                </MapView.Callout>
              </MapView.Marker>
            )

          })}
          </MapView>
        </View>
      )
    } else {
      return (
        <View style={styles.mainContainer}>
          <LoadingModal loading={this.state.isLoading} cancelAction={this.cancelFetchRequest.bind(this)} showCancelButton={this.state.showCancelButton} />

          <ListView
            style={{flex:1, marginBottom: 30}}
            dataSource={this.state.dataSource}
            navigator={this.props.navigator}
            renderRow={(service) => this.renderRow(service)}
            renderSeparator={(sectionID, rowId) => <Separator key={rowId} />}
            renderFooter={() => this.renderFooterPls()}
          />
        </View>
      )
    }
  }

  render(){

    return (
      <View style={styles.mainContainer}>
        <View style={styles.mastheadControlsContainer}>
          <SegmentedControlIOS
            values={['List', 'Map']}
            selectedIndex={0}
            tintColor={'#ffffff'}
            onValueChange={(val) => {
              this.setState({
                selectedSegment: val
              })
            }}
          />
          <RangeSlider
            min={5}
            max={200}
            step={10}
            handleValueChange={this.handleValueChange.bind(this)}
            handleSliderChange={this.handleSliderChange.bind(this)}
            sliderValue={this.state.sliderValue}
          />
        </View>
        {this.renderError()}
        {this.renderPageView()}
      </View>
    )
  }
}

reactMixin(NearMe.prototype, TimerMixin);


module.exports = NearMe;
