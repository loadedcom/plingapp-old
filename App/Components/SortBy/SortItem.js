var React = require('react');
var ReactNative = require('react-native');

var {
  View,
  Text,
  TouchableOpacity,
  StyleSheet
} = ReactNative;

var styles = StyleSheet.create({
  button: {
    marginRight: 10
  }
});

class SortItem extends React.Component {
  constructor(props) {
    super(props);

  }

  render(){
    return (
      <TouchableOpacity
        style={styles.button}
        onPress={this.props.onPress}
      >
        <Text>{this.props.title}</Text>
      </TouchableOpacity>
    )
  }
}

module.exports = SortItem;
