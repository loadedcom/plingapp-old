var React = require('react');
var ReactNative = require('react-native');

var SortItem = require('./SortItem');

var {
  View,
  StyleSheet,
  Text
} = ReactNative;

var styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    backgroundColor: '#5FBB99'
  }
})

class SortBy extends React.Component {
  constructor(props) {
    super(props);

  }

  render(){
    return (
      <View style={styles.container}>
        <Text>Sort by:</Text>
        {this.props.children}
      </View>
    )
  }
}

module.exports = SortBy;
