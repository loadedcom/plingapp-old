var React = require('react');
var ReactNative = require('react-native');
var Slider = require('react-native-slider');

var {
  StyleSheet,
  Text,
  View,
  PropTypes,
} = ReactNative;

var styles = StyleSheet.create({
  sliderValue: {
    color: '#07563D',
    fontSize: 12,
    fontWeight: 'bold',
    textAlign: 'center'
  },
  sliderBookend: {
    color: '#ffffff',
    fontSize: 12,
    fontWeight: 'bold'
  },
});

class RangeSlider extends React.Component {
  constructor(props) {
    super(props);


  };

  render(){
    return (
      <View>
        <View style={{flex:1, flexDirection: 'row', alignItems: 'center'}}>
          <Text style={[styles.sliderBookend]}>{this.props.min}km</Text>
          <View style={{flex:1, marginLeft: 20, marginRight: 20}}>
            <Slider
              value={this.props.sliderValue}
              onValueChange={this.props.handleValueChange}
              onSlidingComplete={this.props.handleSliderChange}
              step={this.props.step}
              minimumTrackTintColor="#ffffff"
              maximumTrackTintColor="#99D2C0"
              thumbTintColor="#ffffff"
              thumbStyle={{
                shadowColor: '#000000',
                shadowOffset: {width: 0, height: 2},
                shadowOpacity: 0.3,
                shadowRadius: 2
              }}
              minimumValue={this.props.min}
              maximumValue={this.props.max}
            />
          </View>
          <Text style={styles.sliderBookend}>{this.props.max}km+</Text>
        </View>
        <Text style={styles.sliderValue}>Services within: {this.props.sliderValue}km</Text>
      </View>
    )
  }

};

module.exports = RangeSlider;
