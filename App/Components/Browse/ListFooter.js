var React = require('react');
var ReactNative = require('react-native');

var {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  ActivityIndicatorIOS
} = ReactNative;

var styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 8,
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: 65
  },
  button: {
    borderColor: '#8E8E8E',
    borderWidth: StyleSheet.hairlineWidth,
    borderRadius: 5,
    marginTop: 10,
  },
  text: {
    color: '#8E8E8E',
  },
  padding: {
    paddingHorizontal: 20,
    paddingVertical: 10,
  }
});

class ListFooter extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    var renderLoading;
    var renderButton;

    if (this.props.loadingMore) {
      renderLoading = (
        <View style={styles.padding}>
          <ActivityIndicatorIOS animating={true} color="#8E8E8E" size="small" />
        </View>
      )
    }

    if ((this.props.showingNumberOfPosts != this.props.totalPostsFound) && !this.props.loadingMore) {
      renderButton = (
        <TouchableOpacity style={[styles.button,styles.padding]} onPress={this.props.action}>
          <Text style={styles.text}>Load More</Text>
        </TouchableOpacity>
      )
    }

    return (
      <View style={styles.container}>
        <View>
          <Text style={[styles.text,{fontSize: 10}]}>Displaying {this.props.showingNumberOfPosts} of {this.props.totalPostsFound} results</Text>
        </View>
        {renderLoading}
        {renderButton}
      </View>
    );
  }
}

module.exports = ListFooter;
