var React = require('react');
var ReactNative = require('react-native');
var Icon = require('react-native-vector-icons/Ionicons');

var {
  View,
  Text,
  StyleSheet,
} = ReactNative;

var styles = StyleSheet.create({
  paragraph: {
    color: '#757575',
    fontSize: 12,
    marginBottom: 10,
    marginTop: 10,
  },
  iconInfo: {
    color: '#757575',
    fontSize: 10,
    marginLeft: 4
  }

});

class ServiceInfoIcon extends React.Component {

  constructor(props){
    super(props);
    this.state = {
      isLoading: false,
      error: false
    }
  }

  render() {
    return (
      <View style={{flex: 1, flexDirection: 'row',}}>
        <Icon name={this.props.icon} size={13} color="#5B5B5B" />
        <Text style={styles.iconInfo}>
          {this.props.text}
        </Text>
      </View>
    )
  }
}

module.exports = ServiceInfoIcon;
