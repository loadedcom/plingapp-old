var React = require('react');
var ReactNative = require('react-native');
var Separator = require('../Separator');
var SingleCategory = require('./SingleCategory');

var Entities = require('html-entities').XmlEntities;
var api = require('../../Utils/Api');

var {
  View,
  Text,
  TouchableHighlight,
  TouchableOpacity,
  PropTypes,
  Image,
  StyleSheet
} = ReactNative;

var styles = StyleSheet.create({
  button: {
    backgroundColor: '#FBFBFB',
    padding: 20,
    paddingTop: 10,
    paddingBottom: 10,
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent:'center',
  }
})

class CategoryRow extends React.Component {

  constructor(props){
    super(props);

    this.state = {
      thumbUrl: ''
    }
  }

  handleSelectArticle() {
    this.props.navigator.push({
      title: Entities.decode(this.props.rowInfo.name),
      backButtonTitle: 'Back',
      component: SingleCategory,
      passProps: {
        slug: this.props.rowInfo.slug,
        count: this.props.rowInfo.count
      }
    });
  }

  getThumb(id) {

    api.getCategoryThumb(id)
    .then((res) => {
      this.setState({
        thumbUrl: res.image.url
      })
    })
  }

  componentWillMount() {
    this.getThumb(this.props.rowInfo.id);
  }

  render() {

    return (
      <View>
        <TouchableOpacity
          onPress={this.handleSelectArticle.bind(this)}
        >
          <View style={styles.button}>
            <Image
              source={{uri: this.state.thumbUrl, width: 40, height: 40}}
              style={{width: 40, height: 40}}
            />
            <Text style={{flex: 1, marginLeft: 10}}>{Entities.decode(this.props.rowInfo.name)}</Text>
            <Image
              source={require('./images/row-arrow-next.png')}
              style={{width: 12, height: 20}}
            />
          </View>
        </TouchableOpacity>
        <Separator />
      </View>
    )
  }
}

CategoryRow.propTypes = {
  rowInfo: React.PropTypes.object.isRequired,
};

module.exports = CategoryRow;
