var React = require('react');
var ReactNative = require('react-native');
var api = require('../../Utils/Api');
var ServiceDetail = require('../ServiceDetails/ServiceDetail');
var ServiceInfoIcon = require('./ServiceInfoIcon');
var tags = require('../../Utils/stripTags');

var Entities = require('html-entities').XmlEntities;

var geodist = require('geodist');

var {
  View,
  Text,
  TouchableHighlight,
  TouchableOpacity,
  PropTypes,
  StyleSheet,
  Image
} = ReactNative;

var styles = StyleSheet.create({
  button: {
    backgroundColor: '#FBFBFB',
    padding: 20,
    paddingTop: 10,
    paddingBottom: 10,
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent:'center',
  },
  paragraph: {
    color: '#757575',
    fontSize: 14,
    marginBottom: 10,
    marginTop: 10,
  },
  iconInfo: {
    color: '#757575',
    fontSize: 10,
    marginLeft: 4
  },
  iconsContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    width:160
  }

})

class ServiceRow extends React.Component {

  constructor(props){
    super(props);
    this.state = {
      isLoading: false,
      error: false
    }
  }

  getAgeRange() {
    var max = this.props.postInfo.acf.maxage;
    var min = this.props.postInfo.acf.minage;

    if (max == 999) {
      max = '+'
    } else {
      max = '-' + max
    }

    if (max == '+' && min == 0) {
      return 'All Ages'
    } else {
      return min + max;
    }
  }

  createExcerpt() {
    var content = tags.stripTags(this.props.postInfo.content.rendered);
    var maxLength = 120 // maximum number of characters to extract
    //trim the string to the maximum length
    var trimmedString = content.substr(0, maxLength);

    //re-trim if we are in the middle of a word
    trimmedString = trimmedString.substr(0, Math.min(trimmedString.length, trimmedString.lastIndexOf(" ")))

    return Entities.decode(trimmedString);
  }

  render() {
    var enableDistanceIcon = this.props.enableDistanceIcon;
    var distanceIcon;

    if (enableDistanceIcon) {

      var lat = this.props.postInfo.acf.address[0].map.lat;
      var long = this.props.postInfo.acf.address[0].map.lng;

      var totalDistance = geodist({lat: lat, lon: long}, {lat: this.props.mylat, lon: this.props.mylng}, {unit: 'km'})

      distanceIcon = <ServiceInfoIcon text={totalDistance + 'km'} icon="ios-navigate" />;
    }

    return (
      <View>
        <TouchableOpacity
          onPress={this.props.action}
        >
          <View style={styles.button}>
            <View style={{flex: 1,marginRight: 30}}>
              <Text>{Entities.decode(this.props.postInfo.title.rendered)}</Text>
              <Text style={styles.paragraph}>
                {this.createExcerpt()}&hellip;
              </Text>
              <View style={styles.iconsContainer}>
                <ServiceInfoIcon text={this.getAgeRange()} icon="ios-person" />
                {distanceIcon}
              </View>
            </View>

            <Image
              source={require('./images/row-arrow-next.png')}
              style={{width: 12, height: 20}}
            />
          </View>

        </TouchableOpacity>
      </View>
    )
  }
}

ServiceRow.propTypes = {
  postInfo: React.PropTypes.object.isRequired,
  // navigator: React.PropTypes.object.isRequired,
};

module.exports = ServiceRow;
