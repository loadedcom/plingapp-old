var React = require('react');
var ReactNative = require('react-native');
var CategoryRow = require('./CategoryRow');
var SingleCategory = require('./SingleCategory');
var api = require('../../Utils/Api');
var SearchBar = require('../SearchBar');
var LoadingModal = require('../LoadingModal');

var ErrorOffline = require('../Errors/Offline/ErrorOffline');

var Fabric = require('react-native-fabric');
var { Answers } = Fabric;
var { Crashlytics } = Fabric;


var {
  View,
  ScrollView,
  ActivityIndicatorIOS,
  StyleSheet,
  AlertIOS,
  NavigatorIOS,
  TouchableOpacity,
  Image,
  TextInput,
  Text
} = ReactNative;

var styles = StyleSheet.create({
  title: {
    fontWeight: '500',
  },
  mapContainer: {
    flex: 1,
  },
  mainContainer: {
    backgroundColor: '#FBFBFB',
    flex: 1,
    flexDirection: 'column',
    marginBottom: 64
  }
});

class Categories extends React.Component {

  constructor(props){
    super(props);

    this.state = {
      isLoading: true,
      error: false,
      categories: [],
      networkError: false
    }
  }

  cancelFetchRequest() {
    this.setState({
      showCancelButton: false,
      isLoading: false,
    });

    console.log('Logging to Answers: Cancelled Fetch: Component: Categories')
    Answers.logCustom('Cancelled Fetch', {Component: 'Categories'});
  }

  retryFetchRequest() {
    this.setState({
      showCancelButton: false,
      isLoading: false,
    });
    setTimeout(() => {this.getCategories()}, 100);
  }

  RenderNetworkError() {
    if (this.state.networkError) {
      return (
        <ErrorOffline retryAction={() => this.getCategories()} message="Your connection to the server was lost"></ErrorOffline>
      )
    }
  }

  getCategories() {
    this.setState({
      error: null,
      isLoading: true
    });

    setTimeout(() => {this.setState({showCancelButton: true})}, 25000);

    api.getCategories()
    .then((res) => {
      this.setCategoryListState(res);
    })
    .catch(
      (e) => {
        this.setState({
          error: 'Network request failed',
          networkError: true,
          isLoading: false,
        })
        console.log(e);
      }
    );
  }

  setCategoryListState(categories){
    this.setState({
      categories: categories,
      isLoading: false
    });
  }

  componentDidMount() {
    this.getCategories();
  };

  render(){
    var cats = this.state.categories;
    var list = cats.map((item, index) => {
      return (
        <CategoryRow key={index} rowInfo={cats[index]} count={cats[index].count} slug={cats[index].slug} navigator={this.props.navigator} />
      )
    });

    return (
      <ScrollView style={styles.mainContainer}>

        <LoadingModal loading={this.state.isLoading} cancelAction={this.cancelFetchRequest.bind(this)} showCancelButton={this.state.showCancelButton} />

        <Image source={require('./images/Banner@2x.png')} resizeMode='cover' style={{height: 156}} />

        <SearchBar navigator={this.props.navigator} />

        {list}

        <View style={{marginTop: 30}}>
        {this.RenderNetworkError()}
        </View>

      </ScrollView>
    )
  }
}

module.exports = Categories;
