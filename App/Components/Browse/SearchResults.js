var React = require('react');
var ReactNative = require('react-native');
var SearchBar = require('../SearchBar');
var LoadingModal = require('../LoadingModal');
var api = require('../../Utils/Api');

var ServiceRow = require('./ServiceRow');
var ServiceDetail = require('../ServiceDetails/ServiceDetail');
var ListFooter = require('./ListFooter');
var Separator = require('../Separator');

var ErrorOffline = require('../Errors/Offline/ErrorOffline');

var Fabric = require('react-native-fabric');
var { Answers } = Fabric;
var { Crashlytics } = Fabric;

var {
  View,
  Text,
  TouchableHighlight,
  ScrollView,
  ActivityIndicatorIOS,
  ListView,
  NavigatorIOS,
  StyleSheet
} = ReactNative;

var styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: '#EBEBEB'
  },
  button: {
    backgroundColor: '#8F1F9F',
    color: '#ffffff',
    padding: 20,
  },
  errorContainer: {
    flex: 1,
    padding: 20
  }
});

class SearchResults extends React.Component {

  constructor(props){
    super(props);

    const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});

    this.state = {
      isLoading: true,
      error: false,
      dataSource: new ListView.DataSource({
        rowHasChanged: (row1, row2) => row1 !== row2,
      }),
    }
  }

  RenderNetworkError() {
    if (this.state.networkError) {
      return (
        <ErrorOffline retryAction={() => this.getSearchResults()} message="Your connection to the server was lost"></ErrorOffline>
      )
    }
  }

  getSearchResults() {
    this.setState({
      isLoading: true
    });

    api.getServicesBySearch(this.props.searchQuery)
    .then((res) => {
      if (res.length == 0) {
        this.setState({
          isLoading: false,
          error: 'no posts found'
        })
        console.log(this.state.error);
      } else {
        this.setState({
          dataSource: this.state.dataSource.cloneWithRows(res),
          isLoading: false
        });
      }
    })
    .catch(
      (e) => {
        console.log(e);
        this.setState({
          error: 'Network request failed',
          networkError: true,
          isLoading: false,
        });
      }
    )
  }

  handleSelectArticle(service) {
    this.props.navigator.push({
      title: service.title.rendered,
      backButtonTitle: 'Back',
      component: ServiceDetail,
      passProps: {
        serviceInfo: service
      }
    });
  }

  renderRow(service) {
    return <ServiceRow action={this.handleSelectArticle.bind(this, service)} key={service.id} postInfo={service} />
  }

  componentDidMount() {
    this.getSearchResults();
    console.log('Answers: LogSearch ' + this.props.searchQuery)
    Answers.logSearch(this.props.searchQuery);
  }

  render() {


    return (
      <ScrollView style={styles.mainContainer}>
        <LoadingModal loading={this.state.isLoading} />

        <ListView
          style={{flex:1, marginBottom: 30}}
          dataSource={this.state.dataSource}
          navigator={this.props.navigator}
          renderRow={(service) => this.renderRow(service)}
          renderSeparator={(sectionID, rowId) => <Separator key={rowId} />}
        />
        {this.RenderNetworkError()}
      </ScrollView>
    )
  }
}

SearchResults.propTypes = {
};

module.exports = SearchResults;
