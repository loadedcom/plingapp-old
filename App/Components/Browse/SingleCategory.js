var React = require('react');
var ReactNative = require('react-native');
var SearchBar = require('../SearchBar');
var LoadingModal = require('../LoadingModal');

var ServiceRow = require('./ServiceRow');
var ServiceDetail = require('../ServiceDetails/ServiceDetail');
var ListFooter = require('./ListFooter');
var Separator = require('../Separator');

var Entities = require('html-entities').XmlEntities;
var _ = require('underscore');

var api = require('../../Utils/Api');

var ErrorOffline = require('../Errors/Offline/ErrorOffline');

var Fabric = require('react-native-fabric');
var { Answers } = Fabric;
var { Crashlytics } = Fabric;

var {
  View,
  Text,
  TouchableHighlight,
  ScrollView,
  TouchableOpacity,
  ActivityIndicatorIOS,
  NavigatorIOS,
  ListView,
  StyleSheet
} = ReactNative;

var styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: '#EBEBEB',
    marginBottom: 10
  },
  button: {
    backgroundColor: '#8F1F9F',
    color: '#ffffff',
    padding: 20,
  },
  errorContainer: {
    flex: 1,
    padding: 20
  }
});

class SingleCategory extends React.Component {

  constructor(props){
    super(props);

    const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});

    this.state = {
      isLoading: true,
      error: false,
      loadingMore: false,
      queryNumber: 1,
      showingNumberOfPosts: 10,
      data: [],
      listFooterVisible: false,
      dataSource: new ListView.DataSource({
        rowHasChanged: (row1, row2) => row1 !== row2,
      }),
    }
  }

  renderFooterPls() {
    if (this.state.listFooterVisible) {
      return (
        <ListFooter
          loadingMore={this.state.loadingMore}
          totalPostsFound={this.props.count}
          showingNumberOfPosts={this.state.showingNumberOfPosts}
          action={this.requestMorePosts.bind(this)}
        />
      )
    }
  }

  requestMorePosts() {
    if (!this.state.loadingMore) {
      this.getMorePosts();
    }
  }

  cancelFetchRequest() {
    this.setState({
      showCancelButton: false,
      isLoading: false,
    });

    console.log('Logging to Answers: Cancelled fetch: SingleCategory ' + this.props.slug)
    Answers.logCustom('Cancelled Fetch', {Component: 'SingleCategory ' + this.props.slug});
  }

  retryFetchRequest() {
    this.setState({
      showCancelButton: false,
      isLoading: false,
    });
    setTimeout(() => {this.getPosts();}, 100);

  }

  RenderNetworkError() {
    if (this.state.networkError) {
      return (
        <ErrorOffline retryAction={() => this.getPosts(1)} message="Your connection to the server was lost"></ErrorOffline>
      )
    }
  }

  getPosts(pageNumber) {
    this.setState({
      isLoading: true
    });

    console.log();

    setTimeout(() => {this.setState({showCancelButton: true})}, 5000);

    api.getSingleCategory(this.props.slug, this.state.queryNumber)
    .then((res) => {
      if (res.length == 0) {
        this.setState({
          isLoading: false,
          error: 'no posts found'
        })
        console.log(this.state.error);
      } else {
        this.setState({
          dataSource: this.state.dataSource.cloneWithRows(res),
          data: res,
          queryNumber: this.state.queryNumber + 1,
          isLoading: false,
          listFooterVisible: true
        });
      }
    })
    .catch(
      (e) => {
        this.setState({
          error: 'Network request failed',
          networkError: true,
          isLoading: false,
        })
        console.log(e);
      }
    );
  }

  getMorePosts(pageNumber) {
    this.setState({
      loadingMore: true
    });
    console.log('getting page:' + this.state.queryNumber );

    api.getSingleCategory(this.props.slug, this.state.queryNumber)
    .then((res) => {
      if (res.length == 0) {
        this.setState({
          loadingMore: false,
          error: 'no posts found'
        })
        console.log(this.state.error);
      } else {
        var oldList = this.state.data;

        var newList = oldList.concat(res);

        this.setState({
          dataSource: this.state.dataSource.cloneWithRows(newList),
          data: newList,
          showingNumberOfPosts: newList.length,
          queryNumber: this.state.queryNumber + 1,
          loadingMore: false
        });
      }
    })
  }

  handleSelectArticle(service) {
    this.props.navigator.push({
      title: Entities.decode(service.title.rendered),
      backButtonTitle: 'Back',
      component: ServiceDetail,
      passProps: {
        serviceInfo: service
      }
    });
  }

  requestMorePosts() {
    if (!this.state.loadingMore) {
      this.getMorePosts();
    }
  }

  renderRow(service) {
    return <ServiceRow action={this.handleSelectArticle.bind(this, service)} key={service.id} postInfo={service} />
  }

  componentDidMount() {
    this.getPosts();
    Answers.logContentView('Category: ' + this.props.slug);
  }

  renderContent() {
    if (this.state.error) {
      return (
        this.RenderNetworkError()
      )
    } else {
      return (
        <ListView
          style={{flex:1, marginBottom: 30}}
          dataSource={this.state.dataSource}
          navigator={this.props.navigator}
          renderRow={(service) => this.renderRow(service)}
          renderSeparator={(sectionID, rowId) => <Separator key={rowId} />}
          renderFooter={() => this.renderFooterPls()}
        />
      )
    }
  }

  render() {

    return (
      <View style={styles.mainContainer}>
        <LoadingModal loading={this.state.isLoading} cancelAction={this.cancelFetchRequest.bind(this)} showCancelButton={this.state.showCancelButton} />

        <View style={{height: 40}}>
          <SearchBar navigator={this.props.navigator} />
        </View>

        {this.renderContent()}

      </View>
    )
  }
}

SingleCategory.propTypes = {
  navigator: React.PropTypes.object.isRequired,
};


module.exports = SingleCategory;
