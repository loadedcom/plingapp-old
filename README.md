## Requirements

1. [Homebrew](http://brew.sh/) to install [watchman](https://facebook.github.io/watchman/docs/install.html).
2. [Node.js](https://nodejs.org/) 4.0 or newer.
3. XCode

## Quick start
- `brew install watchman`
- `npm install -g react-native-cli` (installs React Native command line tools)
- `npm install`
- `react-native run-ios` **OR** open `ios/AwesomeProject.xcodeproj` and hit "Run" button in Xcode

**To run on iOS device:**
See [Running on iOS Device page](docs/running-on-device-ios.html#content)